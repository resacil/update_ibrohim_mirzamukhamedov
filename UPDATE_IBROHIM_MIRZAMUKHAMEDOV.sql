-- Updating added new film duration and rate
UPDATE "film" 
SET "rental_duration" = 21, "rental_rate" = 9.99
WHERE "title" = 'Bridge to terabithia';

-- Updating customer info to mine
UPDATE "customer"
SET "first_name" = 'Ibrohim',
    "last_name" = 'Mirzamukhamedov',
    "email" = 'Ibrohim_Mirzamukhamedov@student.itpu.uz',
    "phone_number" = '+998909187094'
    "address_id" = (SELECT "address_id" 
					FROM "address" 
					WHERE "address" = '1190 0 Place')  
WHERE "customer_id" = ( SELECT customer.customer_id
                        FROM customer
                        JOIN rental ON customer.customer_id = rental.customer_id
                        JOIN payment ON customer.customer_id = payment.customer_id
                        GROUP BY customer.customer_id
                        HAVING COUNT(rental.rental_id) >= 10 
                        AND COUNT(payment.payment_id) >= 10
	                    limit(1));
	
--Updating the customer's create_date value to current_date
UPDATE "customer"
SET "create_date" = CURRENT_DATE;


